import React from 'react';
// import logo from './logo.svg';
// import './App.css';

import './assets/bootstrap/css/bootstrap.min.css'
import './assets/css/style.css'
import './assets/fonts/font-awesome.min.css'

import Routes from './Routes';


function App() {
  return (
    <Routes/>
  );
}

export default App;
