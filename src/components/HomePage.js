import React, { Component } from 'react';
import Card from './Card';


class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [
                {
                    img: require('../assets/img/Icon1.png'),
                    title: "MOBILE APPLICATION DEVELOPMENT",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                },
                {
                    img: require('../assets/img/icon2.png'),
                    title: "ENTERPRISE APPLICATION DEVELOPMENT",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                },
                {
                    img: require('../assets/img/icon3.png'),
                    title: "BRAND IDENTITY",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                },
                {
                    img: require('../assets/img/icon4.png'),
                    title: "IOT DEVELOPMENT",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                },
                {
                    img: require('../assets/img/icon5.png'),
                    title: "BIG DATA SERVICES",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                },
                {
                    img: require('../assets/img/icon6.png'),
                    title: "CLOUD SERVICES & SOLUTIONS",
                    para: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."
                }
                
            ]
        }
    }
    render() { 
        return (
            <div class="banner-withText">
    <div class="w-100 h-100 pr-3 pl-3">

  <header>

    <div class="logo">
      <a href="javascript:void(0);">
        <img style={{width: 120}} src={require('../assets/img/Logo.png')} alt=""/>
      </a>
    </div>

    <div class="social-links">
      
      <ul>
        <li><a href="javascript:void(0);"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
        <li><a class="text-uppercase" href="mailto:@info@nexuscorpltd.ae"><i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp; info@nexuscorpltd.ae</a></li>
      </ul>

      <div>

        <div class="inner">
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link active" href="#">HOME</a>
            <a class="nav-link" href="#">SERVICES</a>
            <a class="nav-link" href="#">PORTFOLIO</a>
            <a class="nav-link" href="#">CLIENTS</a>
            <a class="nav-link" href="#">AWARDS</a>
            <a class="nav-link" href="#">CONTACT</a>
            <a class="nav-link bg-white b-radius-25 text-black" href="tel:8000321770"><i class="fa fa-volume-control-phone" aria-hidden="true">
            </i>&nbsp;&nbsp;800 032 1770</a>
          </nav>
        </div>
    
      </div>
    </div>
    
  </header>
  </div>

  <main role="main" class="inner cover text-black">
  <h1 class="cover-heading">Lets's Talk</h1>
    <h2 class="lead">Ideas</h2>
     <p class="lead">
      Dubai Leading <strong>Digital Agency</strong><br/>
      Just Got Better
    </p>
    <p class="lead">
      <a href="#" class="btn btn-lg btn-secondary text-uppercase btn-text">Get Started</a>
    </p>

    <div class="arrow-down">
      <a href="#content-2"><img src={require('../assets/img/arrow.png')} alt=""/></a>
    </div>
  </main>


  <section id="content-2" class="content-2 row">
    <div class="col-md-5">

      <div class="content-2-left">

        <div class="text-content">
          <h1 class="fade-out">Services</h1>
          <h1 class="cover-heading">Services</h1>
          <h2 class="cover-heading-small"> <span class="we">We</span> Offer</h2>
          <p class="cover-heading-p">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>
          
        </div>
        <div class="arrows">
          <i class="fa fa-long-arrow-left pr-3" aria-hidden="true"></i>
          <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
        </div>
        

        

      </div>

    </div>
    <div class="col-md-7">
      
      <div class="content-2-right row">

          {
              this.state.cards.map((card, i)=>{
                return <Card img={card.img} title={card.title} para={card.para}/>
              })
              
          }

        {/* <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
              <img style={{width: 70}} src={require('../assets/img/Icon1.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div>
        
        <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
            <img style={{width: 70}} src={require('../assets/img/icon2.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div>
        
        <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
            <img style={{width: 70}} src={require('../assets/img/icon3.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div>

        <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
            <img style={{width: 70}} src={require('../assets/img/icon4.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div>

        <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
            <img style={{width: 70}} src={require('../assets/img/icon5.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div>

        <div class="col-md-4">

          <div class="card" >
            <div class="card-header">
            <img style={{width: 70}} src={require('../assets/img/icon6.png')} alt=""/>
            </div>
            <div class="card-body">
              <h5 class="card-title">Mobile Application Development</h5>
              <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
              <hr class="card-hr"/>
            </div>
          </div>

        </div> */}

        
      </div>

    </div>
  </section>

  

  </div>
        );
    }
}
 
export default HomePage;