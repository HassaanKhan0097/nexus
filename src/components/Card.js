import React, { Component } from 'react';


class Card extends Component {
    constructor(props) {
        super(props);
    }
    render() { 
        return (
            <div class="col-md-4">

                <div class="card" >
                    <div class="card-header">
                    <img style={{width: 70}} src={this.props.img} alt=""/>
                    </div>
                    <div class="card-body">
                    <h5 class="card-title">{this.props.title}</h5>
                    <p class="card-text">{this.props.para}</p>
                    <hr class="card-hr"/>
                    </div>
                </div>

        </div>
        
        );
    }
}
 
export default Card;